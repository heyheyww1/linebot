# -*- coding: utf-8 -*-

#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#       https://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

import os
import sys
import requests
import json
import re

from argparse import ArgumentParser
from flask import Flask, request, abort
from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, PostbackEvent,
    TextMessage, TextSendMessage, StickerMessage, ImageSendMessage,
    TemplateSendMessage, ButtonsTemplate,
    PostbackTemplateAction, MessageTemplateAction, URITemplateAction
)

app = Flask(__name__)

# const
SEARCH_BY_POSTAL = 'searchByPostal'
SEARCH_BY_POSTAL_TEXT = '郵便番号→住所検索'
TRANSLATE = 'translate'
TRANSLATE_TEXT = '翻訳'
STATUS_ASK_POSTAL = 'askPostal'
STATUS_ASK_TRANSLATE_SOURCE = 'askTranslateSource'
STATUS_ASK_TRANSLATE_TARGET = 'askTranslateTarget'
STATUS_ASK_TRANSLATE_TEXT = 'askTranslateText'
TRANSLATE_API_KEY = 'AIzaSyDiLvcgycug2c9HlS_1uJmmtzNDrwI7YxE' 
TEXTS = [
    SEARCH_BY_POSTAL_TEXT,
    TRANSLATE_TEXT
]
langs = {
    'ja' : '日本語',
    'en' : 'English',
    'es' : 'espanol'
}

# global
status = {} #やりとりのステータスを保持
lang_source = {} #翻訳元の言語を保存
lang_target = {} #翻訳先の言語を保存

# get channel_secret and channel_access_token from your environment variable
channel_secret = os.getenv('LINE_CHANNEL_SECRET', None)
channel_access_token = os.getenv('LINE_CHANNEL_ACCESS_TOKEN', None)
if channel_secret is None:
    print('Specify LINE_CHANNEL_SECRET as environment variable.')
    sys.exit(1)
if channel_access_token is None:
    print('Specify LINE_CHANNEL_ACCESS_TOKEN as environment variable.')
    sys.exit(1)

# LineBotApi
line_bot_api = LineBotApi(channel_access_token)
handler = WebhookHandler(channel_secret)


@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    print('body' + body)

    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)
    return 'OK'


@handler.add(MessageEvent, message=TextMessage)
def message_text(event):

# debug
    print('TextMesageEvent:' + str(event))

    print(str(event.source))
    user_id = event.source.user_id
    text = event.message.text

    if text == 'さあ':
        res_text_message = TextSendMessage('やろう')
        res_image_message = ImageSendMessage(
                                original_content_url='https://heroku-line-bot-hey2018.herokuapp.com/static/uno1.jpg',
                                preview_image_url='https://heroku-line-bot-hey2018.herokuapp.com/static/uno1.jpg'
                             )
        line_bot_api.reply_message(event.reply_token, [res_text_message, res_image_message])

    elif text.find('こんにちは') > -1:
        line_bot_api.reply_message(event.reply_token, TextSendMessage('こんにちは。「へい、こへ」って呼んでね'))

    elif text.find('ありがとう') > -1:
        line_bot_api.reply_message(event.reply_token, TextSendMessage('どういたしまして'))

    elif text.find('すごい') > -1:
        line_bot_api.reply_message(event.reply_token, TextSendMessage('まあな'))

    elif text.find('にょ') > -1:
        line_bot_api.reply_message(event.reply_token, TextSendMessage('ながお、かわいい'))

    elif text.find('しのびねえな') > -1:
        line_bot_api.reply_message(event.reply_token, TextSendMessage('かまわんよ'))

    elif text.find('最高') > -1:
        line_bot_api.reply_message(event.reply_token, TextSendMessage('だろ？'))

    elif text == 'へい、こへ':
        buttons_template_message = TemplateSendMessage(
            alt_text = 'Hey Kohe',
            template = ButtonsTemplate(
#                thumbnail_image_url='https://heroku-line-bot-hey2018.herokuapp.com/static/uno1.jpg',
                title = '何をしますか？',
                text = '下から選んでください',
                actions = [
                    PostbackTemplateAction(label=SEARCH_BY_POSTAL_TEXT, text=SEARCH_BY_POSTAL_TEXT, data=SEARCH_BY_POSTAL),
                    PostbackTemplateAction(label=TRANSLATE_TEXT, text=TRANSLATE_TEXT, data=TRANSLATE),
                    MessageTemplateAction(label='ありがとう', text='ありがとう'),
                    URITemplateAction(label='Googleで調べる', uri='http://google.co.jp/')
                ]
            )
        )
        line_bot_api.reply_message(event.reply_token, buttons_template_message)

    elif text == 'キャンセル':
        del status[user_id]  #ステータス削除
        del lang_source[user_id] #翻訳元削除
        del lang_target[user_id] #翻訳先削除
        line_bot_api.reply_message(event.reply_token, TextSendMessage('キャンセルしました。またいつでもお呼びください。'))

    elif text in TEXTS:
        return

    else:
        if user_id in status:
            # 住所検索中
            if status[user_id] == STATUS_ASK_POSTAL:
                if re.match('[0-9]{3}-[0-9]{4}' , text):
                    url = 'http://geoapi.heartrails.com/api/json?method=searchByPostal&postal=' + text
                    r = requests.get(url)
                    json_data = json.loads(r.text)
    
                    print(json.dumps(json_data, indent=4)) #debug
    
                    json_data_response = json_data['response']
                    if 'error' in json_data_response:
                        res_text = 'この郵便番号に紐づく住所は見つかりません'
                    else: 
                        location = json_data_response['location'][0]
                        res_text = location['prefecture'] + location['city'] + location['town']
                else:
                    res_text = '郵便番号を入力してください XXX-XXXX'
                line_bot_api.reply_message(event.reply_token, TextSendMessage(res_text))

            # 翻訳先を指定する 
            elif status[user_id] == STATUS_ASK_TRANSLATE_SOURCE:
                # 翻訳元をセットする
                for key, label in langs.items():
                    if label == text:
                        lang_source[user_id] = key

                status[user_id] = STATUS_ASK_TRANSLATE_TARGET
                buttons_template_message = TemplateSendMessage(
                    alt_text = 'translate',
                    template = ButtonsTemplate(
                        title = '翻訳先は？',
                        text = '下から選んでね',
                        actions = [
                            MessageTemplateAction(label = '日本語', text = '日本語'),
                            MessageTemplateAction(label = 'English', text = 'English'),
                            MessageTemplateAction(label = 'Espanol', text = 'Espanol')
                        ]
                    )
                )
                line_bot_api.reply_message(event.reply_token, buttons_template_message)

            # 翻訳先を指定する 
            elif status[user_id] == STATUS_ASK_TRANSLATE_TARGET:
                # 翻訳先をセットする
                for key, label in langs.items():
                    if label == text:
                        lang_target[user_id] = key
                status[user_id] = STATUS_ASK_TRANSLATE_TEXT
                line_bot_api.reply_message(event.reply_token, TextSendMessage('翻訳する文章を入力してください。'))

            # 翻訳し、結果を返す 
            elif status[user_id] == STATUS_ASK_TRANSLATE_TEXT:
                url = 'https://translation.googleapis.com/language/translate/v2?target={}&key={}&source={}&q={}'.format(lang_target[user_id], TRANSLATE_API_KEY, lang_source[user_id], text)
                r = requests.get(url)
                json_data = json.loads(r.text)
                
                print(json.dumps(json_data, indent=4)) #debug
                line_bot_api.reply_message(event.reply_token, TextSendMessage(json_data['data']['translations'][0]['translatedText']))

# Sticker
@handler.add(MessageEvent, message=StickerMessage)
def message_sticker(event):
    res_text = 'かわいい'
    line_bot_api.reply_message(event.reply_token, TextSendMessage(res_text))

@handler.add(PostbackEvent)
def postback(event):

    print('posback_event' + str(event)) # debug
    print('data' + event.postback.data) # debug

    user_id = event.source.user_id
    postback_data = event.postback.data

    print('postback_data is ' + postback_data) # debug

    # 住所検索のとき
    if postback_data == SEARCH_BY_POSTAL:

        res_text = '郵便番号を入れてください'
        status[user_id] = STATUS_ASK_POSTAL #ステータスを変更
        line_bot_api.reply_message(event.reply_token, TextSendMessage(res_text))

    # 翻訳のとき
    elif postback_data == TRANSLATE:
        buttons_template_message = TemplateSendMessage(
            alt_text = 'translate',
            template = ButtonsTemplate(
                title = '翻訳元テキストは？',
                text = '下から選んでね',
                actions = [
                    MessageTemplateAction(label = '日本語', text = '日本語'),
                    MessageTemplateAction(label = 'English', text = 'English'),
                    MessageTemplateAction(label = 'Espanol', text = 'Espanol')
                ]
            )
        )
        status[user_id] = STATUS_ASK_TRANSLATE_SOURCE #ステータスを変更
        line_bot_api.reply_message(event.reply_token, buttons_template_message)

if __name__ == "__main__":
    port = int(os.getenv("PORT", 5000))
    app.run(host="0.0.0.0", port=port)

